import qrcode
qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_L,
    box_size=10,
    border=4,
)
qr.add_data('https://codeberg.org/tonycythony')
qr.make(fit=True)

img = qr.make_image(fill_color="black", back_color="orange")

file = "myrusakov_out_1.png"
img.save(file)